---
title: "Tempo Valence"
date: 2020-07-10T08:15:00+01:00
categories: ["Side Projects"]
tags: ["Docker", "Kubernetes", "Kotlin", "Learning"]
image: /img/music.jpg
author: "mattTea"
draft: false
showtoc: false
---

An api allowing the user to search Spotify's api for music based on its Valence (positivity or happiness), and soon, its Tempo (beats per minute).

------

The code for the `Tempo Valence` app can be found at https://gitlab.com/matt34Tea/tempo-valence
