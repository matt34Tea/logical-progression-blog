---
title: "Adding a Custom Domain to a GitLab Pages Site"
date: 2020-08-30T08:15:00+01:00
categories: ["Posts"]
tags: ["DNS", "Static Site"]
image: /img/network.jpg
author: "mattTea"
draft: false
showtoc: false
---

My `logical-progression` blog site is built using the Hugo static site generator and is hosted on GitLab Pages. With GitLab Pages custom domains are possible, and I was keen to use the logical-progression alias.

Initially, however, I couldn't remember who my DNS host was! A problem, as resource records need to be added there to enable the alias. The problem was based on the fact that...
- The `matttea.com` domain was purchased via Google Domains
- My webhosting was with Interserver

My thinking was that I needed to add the relevant `CNAME` and `TXT` resource records to the Google Domains dashboard (as it was the clearest dashboard to work with), and by doing so I was prompted to use the Google domain nameservers. Doing this broke all my other sites, highlighting that Google obviously wasn't my DNS host!

Realising that Interserver therefore was my DNS host I created the `CNAME` and `TXT` records in the correct place, and reset the nameservers (so that my other sites worked again)
- The `CNAME` or 'canonical name' record maps the alias name to the true or canonical domain name.
- The `TXT` record is required to verify ownership of the domain, giving a gitlab verification code.

The place to add these records on the Interserver site is via the `cPanel > Domains > Zone Editor`, and then by clicking the `MANAGE` option for the domain you want to add a subdomain alias to. In this case `matttea.com`.

As DNS changes can take anything from a few minutes to a few days to propagate, it can be useful to use sites such as `whatsmydns.net` to check in on whether your changes have worked.

GitLab Pages also offers free certificate management with `Let's Encrypt`, allowing you to enable HTTPS for alias names. This can be switched on in the Pages settings. This can be flaky though and I received a few `Something went wrong while obtaining the Let's Encrypt certificate` error messages. Without this, browsers will give 'not secure' messages to visitors, so it's worth getting it working. The troubleshooting guide suggests the following...
  - Go to your project's Settings > Pages.
  - Click Edit on your domain.
  - Click Retry.

This didn't work for me, so the guide continues with...
  - Make sure you have properly set only one CNAME or A DNS record for your domain.
  - Make sure your domain doesn't have an AAAA DNS record.
  - If you have a CAA DNS record for your domain or any higher level domains, make sure it includes letsencrypt.org.
  - Make sure your domain is verified.

  - Turns out I just needed to retry on the first part quite a good few times.

The final step to set up the new alias is to change the `baseUrl` in the Hugo config to the new domain, this enable CSS on the page amongst other things.
