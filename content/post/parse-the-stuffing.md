---
title: "Parse the Stuffing"
date: 2017-12-21T08:15:00+01:00
categories: ["Posts"]
tags: ["WebDev", "Learning"]
image: /img/stuffing.jpg
author: "mattTea"
draft: false
showtoc: false
---

My development learning journey (which has just seen its 1st anniversary) began with looking for a way to solve problems. I generally followed a certain method thinking it was the only way, or the best way. It rarely was, of course. But I’m finally, now, looking for a better way to solve problems. The big things that helped me see this were collaboration and community.



Spending time with our Developing Developers community really highlights how many different ways there are to do things, and how many different styles of programming people have. Conducting and watching demos, pairing and mobbing, all highlighted alternative options that I’ve then been able to investigate and try to understand further. I’m spending a lot more time in framework and library docs because of it, which is great, because I’m building confidence to do this over other help methods.

Through this group I’m also starting to witness real examples of just how quickly things move on in the world of software development. During a React session run by Matt Dowds I noticed him using the `.bind()` method throughout. And shortly afterwards I spotted the same during a pairing session with Ben Hawkins. So, as a conscientious student I noted it down for further investigation. They were calling this method specifically to bind `this` to specific object instances as required. (‘this’ refers to either the global object or a function-specific object, depending how it is called in JavaScript). Very interesting, what is this new magic, and why was it needed?

If I’d been paying more attention in these sessions with Matt and Ben I’d have noticed the approach they used to create classes was different to mine too. Turns out the `React.createClass()` method I have been using is already deprecated. (I know - in a library that’s still ‘new’, right?!) This would auto-bind methods to objects, meaning separately binding `.this()` was unnecessary. However, with the advent of ES6 classes, explicitly pre-binding methods is now required.

So, as a conscientious analyst this time, I’m now thinking if I have to bind each individual method to each instance of an object (which sounds like more work than before), where’s the benefit? Surely something that was better wouldn’t have been deprecated, would it?

So why was `React.createClass()` deprecated?

Well, it turns out that the React team provided `React.createClass()` as a way to create classes in JavaScript, as there was no inherent way to do so when React was first released. Classes have of course since been added to JavaScript, and this is now the preferred and standard way to create components in React. So, farewell auto-binding.



So, with this new knowledge, and a desire to try to stay on top of it, I started putting some of these things into practice. I’ve just finished rewriting this blog app (again), this time in React, and one of the benefits I’ve found is that it is pushing me to use more ES6 features. A couple of the more obvious ones that I’m currently loving are arrow functions (of course), and my new favourite - ternary operators. And a big benefit of rewriting something is having some certainty over the end goal I'm trying to achieve, so playing about with new techniques seems that little bit easier.

Typically though, the biggest challenge I faced with this rewrite was nothing new. It was just trying to parse the html that’s coming back from my content api. Something I so ignorantly took for granted in my html files previously, didn’t seem quite so simple in jsx.

Actually, it turns out it was relatively simple, it just took over a week of infuriating debugging to find the problem.

Various options took me through solutions that opened up XSS vulnerabilities (which actually became tempting after a number of evenings at this), entity decoding escape functions, regular expressions that don’t actually parse any tags, and finally an npm package that parsed html for react apps (react-html-parser).

This last one had to be the answer, surely? Oh, wait… webpack wanted a final say, of course. That say being `Module parse failed Unexpected token: You may need an appropriate loader to handle this file type.` Ugh. Then of course the loader config format will be different to the docs, then the import format will be different again, then it will randomly decide to work. Oh. (Notwithstanding the growing list of yellow ‘deprecated’ warnings in the terminal that seems standard when using webpack…)

But anyway, webpack fun aside, I eventually got everything working, and the new blog version is now available to check out (as you are right now). I’ve also made a few general improvements, and refactored some old stuff. Everything is now fully dynamic, meaning I don’t need to make any code tweaks when I add a blog post to the CMS, and things are generally more consistent, without any of the async/sync hacks. It’s a bit speedier too (although I think a couple of loading animations might be in the pipeline). I’ve added a few extra features since last time as well, including the previous and next buttons. Oh, and if you get a chance to look on a mobile, the menu is far less 2016 now too.



It seems appropriate to end the year and this final blog of 2017 with a resolution. Or two.

Firstly, I definitely need a few new side projects. I’ve been tweaking and rewriting the same two for too long and I need something new to write about.

Secondly, 2018 will be the year I finally convince someone to let me take a dev story and write some actual production code...