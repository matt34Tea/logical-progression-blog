---
title: "The Art of Flânerie"
date: 2017-07-07T08:15:00+01:00
categories: ["Posts"]
tags: ["React", "Analysis", "Learning"]
image: /img/hackathon.jpg
author: "mattTea"
draft: false
showtoc: false
---

I talk a lot about flexible and multi-skilled teams, but rarely in my current role do I get let loose in one any more. Knowing I need more exposure in these teams I got along to couple of events recently for that exact purpose.

A hackathon (my first), turned out to be a pretty intense experience. I won’t lie, I was definitely the odd one out (not being a developer, not working at a start-up, not having a mac covered in stickers…); and the value I brought was thoroughly exhausted 20 mins into the day (if I'm being kind), but I learnt a load.

Jointly hosted by tfl and TicketMaster as part of London Tech Week, the goal was to hack an idea using the data available from their public apis. I joined a small group of 3 from LiveStyled.com with the idea of Gig Roulette.

Trawling the apis and defining practical end points was about as technical as I got, but being involved in design, UX, UI and data conversations (however small) highlighted that we all had something to offer beyond what our job titles might suggest. The area I felt most confident was, unsurprisingly, keeping the focus on the problem and ensuring we kept our product at a minimum viable level before getting too excited with a ton of new features (which always seemed to happen immediately after a coffee…) Probably annoyed everyone no end, but at least we got an end to end journey finished(ish). Turned out we won too!

Gig Roulette took a few user filters - date, maximum travel time, budget and genre; returned a match, took payment (we faked this part), and only then did it tell you where you were going, and provided your route there. Good fun I thought, and definitely something I’d use!



I got along to a BDD session recently too, put on by the good folks at Cucumber.io, where I spent my time as one half of a dev pair(!) We were running through a test driven development cycle, writing automated tests (from the BDD scenarios), writing the code to pass the tests, then refactoring. Again, the experience and knowledge gained by me far outweighed the value I gave, but huge thanks to Jeff from MoogSoft.com for his patience and help.

My initial thoughts were that pushing our BA capability into this realm may not be the right thing to do - inherently technical, coding skills necessary - it would likely mean time away from doing the things our skill set may be more suited to. But on reflection, although I was infinitely slower than Jeff at debugging and writing the stuff, the analysis head did help - looking for completeness, ensuring tests weren’t confirming false positives, edge cases and alternative paths. I left feeling there’s definitely something here, more time doing this in teams needed for me to build on this though.



And more firsts followed… I built my first React component, quickly followed by my first React app, and I spoke at my first tech conference (not forgetting that all important first tech conference laptop sticker).

Now I consider myself a bit of a modern day flâneur, based solely on what I enjoy on holiday, but I’ve started seeing a cross-over in my work too. My tech conference talk tried to describe why this view of our various work societies can be a really beneficial personal and community development model.

To quote Charles Baudelaire... “the flâneur has a key role in understanding, participating in, and portraying society. Not a cynical voyeur, but one who enters into the life of his or her subjects with passion”. The coding exercises and side projects, the hackathons, the conversations, the time on stack overflow, being accepted onto our Tech Council, the books, the blogs, the podcasts; all of this is my flânerie in our devs’ society. And the more I nosed around, the more I was welcomed in and helped; and the more things were highlighted as areas where development in my own community could benefit others.

So I continue to saunter my way through more and more software engineering society alleyways.



And that brings me to some new stuff.

Of course, I’ve updated chunks of this blog app (which I now understand I can expect to do on a never ending basis). Using the great feedback I received after the last post, I’ve fixed a few things that were fairly minimal and just about viable (comments now work properly, and the nav bar is fixed - thanks Steve G), and restructured the app so that social network links are available and share unique pages (thanks Angela W).

Throughout this spring clean I realised I’m well wide of the modular code good practice mark too, so I started looking at my options there. Enter React. The Handlebars vs React conversation has been rumbling on for some time in our ecomm teams, so I thought it useful to build my own opinion.

Initially, I’m really impressed with its simplicity, and how quick it is to be productive. The jsx syntax makes the crossover between mark-up and dynamic code really fluid, and keeps it nicely grouped when splitting out components. And the dev tools, particularly around debugging in the console, are very accessible.

I’m still in very early days, of course, but simple components and simple apps are very quick to get live, and I’ve started splitting out aspects of this blog app through using it, so I’ll be sharing more thoughts as I continue.



This React dalliance has also prompted another couple of side projects. I won't tell you anything about them until I can share something, but with quick demo and feedback cycles in mind I hope this is only a matter of days away.

Inspired by the old dogs, new tricks theme from certain quarters of our recent tech conference (thanks particularly to Mike Neilens and Ian Pletcher), I plan to share these via the iOS App Store (eventually) as well as the web, they will use React throughout, and they will solve actual problems (based on my customer testing with 3 individuals!)