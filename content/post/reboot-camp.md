---
title: "Reboot Camp"
date: 2018-04-26T08:15:00+01:00
categories: ["Posts"]
tags: ["WebDev", "Learning"]
image: /img/reboot-camp.jpg
author: "mattTea"
draft: false
showtoc: false
---

So, the hiatus? A couple of reasons really. A new project at work, and although with an awesome team on a great product, it has meant a few more hours in the office than is ideal. But I also lost the love for my side projects at the same time. I'll try to explain.

I dived into React 16 a little while ago, and super keen to try it out on something more interesting than small tutorial-type challenges, I started another side project. I was, of course, over keen, tried to learn too many things at once, and came crashing down.

As well as picking up lots of lovely ES6 features (bit late to that party, I know), I was keen to use drag and drop (the standard HTML one), local storage and Redux. Hindsight is a beautiful thing, and I can now see that instead of working on one of these areas, getting confident and seeing it do what I want it to do before moving on to the next, I just layered failing code on more failing code. When I found myself in a rabbit hole, instead of backtracking to a working app and trying a different tack, I just tried to build a whole new feature set, hoping it would fix the others. It didn’t. Ever. Of course. Maybe I can blame tiredness, or commitments being elsewhere at that time, but either way I can now at least see it was a good lesson.


The side project, a retro board or trello-type workflow app sadly never saw the light of day. But it will, I’m starting from the ground up again. The old code is in my github repo, and it looked a little like this (in its as yet unstyled state)... 

![Retro board example](/img/retro-board.png)

Working day-to-day in a product team, on a product with a React front end, I had been hoping for an opportunity to pair on a few stories or defects, and see if I could finally put some of my new skills to the test. However, that was before my confidence crumbling hole of a side project, and I did feel pretty knocked, feeling even more the fraud, still with nothing to offer. But as a clever person once said, failure is a more faithful teacher than success (or something similarly motivational), so, to the reboot...



Looking back, the above is a bit harsh, but it’s a pretty accurate description of how I felt. I did, however, manage to learn a few things - the basics of Redux - actions, reducers, basic store management, as well as working elements of drag and drop (and its limitations), so not a complete waste of time. But what this has prompted is a refresh in my learning approach. For one, I’ve decided to join the #100DaysOfCode revolution (http://www.100daysofcode.com/).

For those not familiar, the concept (started by Alexander Kallaway) is to commit to one hour of coding practice every day for 100 consecutive days, and crowdsource the motivation and encouragement needed to keep going. And a huge community now exists around this - one of the few ‘rules’ is that you post each day with the above hashtag, describing what you’ve achieved (or not), and encourage at least 2 others who have done likewise.

Now I’ve not gone all in on that aspect - I don’t really want the anonymous motivating hearts and thumbs just yet, and once I have a goal I can be pretty obsessive about it anyway, so this blog by Sibylle Sehl (https://tinyurl.com/yb4znar6) was a great read, and inspired me to just do what I could and not be too hard on myself. If you can only do 30 minutes one day, do that. If you can’t do anything another day, don’t worry, it’s not a failure, just pick back up when you can.

The thing that most attracted me to this approach was the structure - the daily activity, the planning of bite-sized objectives, and keeping a journal so I could see my progression next time I’m feeling useless. I’ve previously just let my interests lead me, but now I feel like I need a bit of a target, so I’ve set myself some, and I’m writing a few bullet points of what I’ve done each day to make sure I’m heading in the right direction. The main takeaway for me, along Sehl’s lines, is that as long as every time you do something, you make progress towards your objective. For example, if you can’t code, but you can read an article, read one that will add to your knowledge on the subject you’re learning.

I really like the #100DaysOf… concept, and can see how this could benefit different skill sets, all lining up nicely with continuous learning and deliberate practice.



I was also lucky enough to take part in our build camp trial recently. As part of training and supporting all our delivery teams in becoming true engineering teams we are creating and delivering a multi-day camp style training session, aiming to give everyone in a team a hands-on understanding of how we want software to be crafted, deployed and hosted. And the 4 days was an intense whirlwind of continual pair-programming through test-driven development, CI/CD pipelines and cloud hosting. Huge props to Tom and Chris for taking a group with wildly differing technical experience through building a web app in Kotlin, writing the build and deploy scripts in CircleCI, containerising in Docker and hosting in GCP with Kubernetes. No small undertaking, exhausting for everyone, but so much fun! I can't wait for our teams to start seeing this course and getting as excited as we did in bringing it back to the day-to-day.

And some thoughts I took away… I started a whole new language, Kotlin. And it’s not that scary! I don’t know if this is always the case, but after the first few searches for syntax and keywords it already felt familiar, and whilst working through problems it was rarely the actual language that hampered me. In all honesty I don’t think I actually thought about the language itself after the first few hours. I was also worried I would forget everything I’d previously learned in JavaScript when starting out with this (much as I do when trying to speak new languages), but if anything this longer period of time spent writing code and thinking development helped me in all respects. And it worked wonders for my confidence. So much so that I managed to get an action agreed in our retro this week where I'll do some regular pairing with our devs!



So my current goals include some deliberate practice on kata type problems in Kotlin, using a test-driven approach. And I’d like to build something that stitches a React UI onto a Kotlin back-end, just not sure what yet (maybe a workflow app!) Oh, and I'll still be chasing that all important first line of production code.