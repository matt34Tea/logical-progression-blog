---
title: "A PubSub Implementation Pattern"
date: 2022-03-13T15:00:00+01:00
categories: ["Posts"]
tags: ["Learning", "Event-driven Architecture", "GCP"]
image: /img/brain.jpg
author: "mattTea"
draft: true
showtoc: false
---

Describe our design of how pubsub notifiers and events and topics connect via publishers and env vars, etc

Our current design includes the following library components...

- Topic
- Subscription
- Subscriber

And the following additional components to work with the library...

- (Outgoing messages?)
-- Notifiers
-- Events
-- Publishers

- (Incoming messages?)
-- Handler
-- MessageProcessor

------

## What happens for outgoing event notifications?

1. Something triggers an Event (e.g. a request to an endpoint)

2. Higher Order Function `createTopicNotifier` returns a function, which takes the event data and timestamp and publishes the message using a Publisher
   1. Message is created using pubsub builder, data is set on this using the event creator (which takes the event data)
   2. (For my ticket use `.putAttributes(key, value)`on this builder?)
   3. `Publisher.publish(message)`

3. `EventData` is an abstract class which is implemented by classes such as `HierarchiesForProductEventData()`

4. `EventCreator` takes an EventData and timestamp and returns the message body. This is separate from the PubSub attributes (which need to be added to the hierarchies-for-product messages for future timed messages to be sent via the pubsub controller)

------

## For DIGI-xxx ticket

Add `.putAttributes()` into the TopicNotifier message builder, but where should the condition be regarding whether to build message with these attributes or without (as other TopicNotifiers require)
Also for this type of TopicNotifier, pass through a resourceId to be used in pubsub controller (this can be the id from the Event Data implemented class, i.e.HierarchiesForProductEventData.id)

Added a when statement in TopicNotifier to build different message for HierarchiesForProductEventData type