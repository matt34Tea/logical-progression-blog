---
title: "Tiny Gigs"
date: 2017-07-10T08:15:00+01:00
categories: ["Side Projects"]
tags: ["React"]
image: /img/tinygigs.png
author: "mattTea"
draft: false
showtoc: false
---

A simple React web app for live music and theatre in small London venues.

------

The live `TinyGigs` app can be found at http://tinygigs.matttea.com/

The code is at https://github.com/mattTea/TinyGigs
