---
title: "The Tacit Knowledge Series"
date: 2021-04-09T08:15:00+01:00
categories: ["Posts"]
tags: ["WebDev", "Learning"]
image: /img/tacit.jpg
author: "mattTea"
draft: false
showtoc: false
---

From a personal and professional standpoint I'm interested in what is required to keep engineers moving from being experienced and competent to being expert.

Many views point to `tacit knowledge`. But by its very nature tacit knowledge is hard to define and even harder to share.

Shawn Wang describes the problem perfectly in his excellent book **The Coding Career Handbook**...

> *Much of your learning from Junior to Senior involves gaining tacit knowledge. You can read all the programming books in the world, but, by definition, you are still limited to things that people can write down. That is explicit knowledge, and it is only the tip of the iceberg.*


We see tacit knowledge in action every day - when a doctor is able to make a prognosis during a crazy short appointment, or, hopefully more commonly, when an expert engineer looks at a design for all of 5 seconds and can immediately tell you her concerns or questions.
 
In trying to understand how they reached these conclusions so quickly, explanations might start with some principles, then move on to the exceptions and caveats to these principles, and may often ends with something like "*it just feels right*". It's these natural judgements that are made that can't be simply explained so that others could reach the exact same outcome.

For an excellent breakdown of this, with detailed examples in software engineering and other knowledge fields, I highly recommend Cedric Chin's Commonplace article - [Tacit Knowledge is a Real Thing](https://commoncog.com/blog/tacit-knowledge-is-a-real-thing/). 


So I want to tap into the tacit knowledge of those around me. I know I can't replace gaining the experience that will build this knowledge myself, but I hope that by being in tune with it I can see the situations, ask the questions, and hopefully understand the thought processes behind these judgements.

My plan is to pick up on scenarios where experienced engineers might have taken a path through a situation or problem that wasn't obvious to me, pin them down to walk through how they approached it; what experience, principles and ideas they drew on, and then write it down and share it here...

------

### The Tacit Knowledge Series so far

1. [Dependency vulnerabilities](https://logical-progression.matttea.com/post/tacit-vulnerabilities/)
2. [Bigger picture recall](https://logical-progression.matttea.com/post/tacit-bigger-picture-recall/)
3. [Calm context switching](https://logical-progression.matttea.com/post/tacit-context-switching/)