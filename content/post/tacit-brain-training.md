---
title: "Tacit Knowledge Series: 4. Brain Training"
date: 2022-01-08T15:00:00+01:00
categories: ["Posts"]
tags: ["Learning"]
image: /img/brain.jpg
author: "mattTea"
draft: true
showtoc: false
---

*The [Tacit Knowledge Series](https://logical-progression.matttea.com/post/tacit-knowledge-series/) aims to understand expert engineers' approaches and how they reached their judgements in a number of day-to-day situations.*

------

I've recently been reading Dr. Felienne Hermans' [The Programmer's Brain](https://www.manning.com/books/the-programmers-brain), and aside from loving how the simple-to-understand concepts fit nicely alongside my interest in understanding how engineers move from good to great, I'm also super keen to try out some exercises and see the results for myself.

So I guess this article is a slight deviation from the observed day-to-day approaches of my expert peers, instead looking at a slightly different way to build tacit knowledge.  

## Scenario

There's a growing list of ideas from Dr Hermans' excellent book that I'm keen to try out. Here I'll start with 2 exercises that focus on the idea of improving the strength of memories, to improve code comprehension and code production.

(Important because of limitations of STM and need for patterns to be stored and easily retrieved from LTM)

As a concept this has grabbed me, based somewhat on the background and research referenced (example -> retrieval strength is what weakens rather than storage strength, plus...) but also because of the outlining that syntax memory aids flow and removes interruptions and context switching...

(Talk about learning foreign language, talk about deliberate practice, talk about how I've been guilty of assuming it's ok to repeatedly look-up or (*gasp*) copy and paste fairly common code snippets between projects)

2.6 exercise is page 31
Good summary on page 45

## The knowledge

There are two exercises I'm working on right now (I'm sure there will be more very soon)...  

### Code reading memory

The first tests code reading memory (exercise 2.6, page 31)  

### Learning syntax quickly

Why syntax knowledge is important
- Fewer interruptions and better flow
- Leads to greater productivity, and a greatly reduced cognitive load when returning to a problem after the interruption 
- Strengthening memory retrieval (here?)  

And how to improve syntax knowledge (flash cards)

My flashcards to start with...
- Working with Kotlin maps (look at categoryMap)

------
### Flashcard 1.  

*PROMPT*:  
```text
Create a fake (http4k) server in a test (in a JUnit test)
```  
------  
*CARD*:  
```kotlin
private val fakeApp = routes(
    "fake/api/rest/url" bind Method.POST to {
        Response(Status.OK)
            .body("""{
                "field1": "value1",
                "field2": "value2"
            }""".trimIndent())
    }
)

private val fakeService = fakeApp.asServer(Jetty(0)).start()
```  
------
### Flashcard 2.  

*PROMPT*:
```text
Create a fixed clock in Kotlin
```  
------  
*CARD*:  
```kotlin
val timestamp: Instant = Instant.parse("2020-01-01T00:00:00.00Z")

val fixedClock: Clock = Clock.fixed(timestamp, ZoneId.of("UTC"))
```  
------
### Flashcard 3.  

*PROMPT*:  
```text
Create a datastore entity from Kotlin project
```  
------  
*CARD*:  
```kotlin
val key = datastore
    .newKeyFactory()
    .setKind("MyKind")
    .newKey("12345")

val entity = Entity.newBuilder(key)
    .set("fieldName", "fieldValue")
    .build()

datastore.put(entity)
```  
------

How often to study flashcards
- Share the study about retrieval strength being the part of a memory that decays, and how studies have highlighted the ideal interval to practice retrieval (and why retrieval is the best thing to practice)