---
title: "The Picture of Disguised BAs"
date: 2017-09-30T08:15:00+01:00
categories: ["Posts"]
tags: ["Skills", "Analysis"]
image: /img/pigeon-hole.jpg
author: "mattTea"
draft: false
showtoc: false
---

"The aim of life is self-development. To realise one's nature perfectly." Quoth Oscar Wilde's decadent libertine, Lord Henry Wotton.

And Harry’s onto something here. Taking his comment to its obvious natural conclusion in describing 21st century tech skills, “one’s nature” probably doesn't sit perfectly within a generic job description. And if it does, it probably won’t stay there for long.

Disappointed again by a visiting company’s product team telling me "we don't have BAs" as if it's the silver bullet to delivery throughput perfection, I’m reminded why I feel so strongly about skills over job titles.

_So tell me, your organisation conducts precisely zero business analysis? Your analytics teams, product owners, data insight analysts, agile coaches, scrum masters and designers… not a single analytical insight from any of them?_

Forget the new names, look at the skills. Of course this company conducts business analysis, they’ve just grouped the skills differently and shifted their role boundaries about a bit.

But why debate names and boundaries? Let people develop the skills they’re interested in, where it’s needed for a team to deliver, however we want to label each other. Know our team members’ interests and nature, and give them autonomy and empowerment over their own opportunities. Who wouldn’t want that? I’m certain we’ll all be happier, more productive, and never short of an <insert newest fashionable role name> again.



This was very much the theme of my talk this month at the European Business Analysis Conference. I described our concept of a Minimum Viable Team, which focuses on the skills needed to deliver the smallest piece of releasable value, then building the smallest possible team with these skills. Individuals having a strong interest in a particular new skill is essential for this to succeed, as each of us will need to step out of our comfort zones and be accountable for areas we are not yet experts in.

As a BA Practice, we're making progress understanding where people have interests and skills outside our core business analysis capability, and giving them opportunities to learn and use them in teams with these demands (with, of course, excellent support from our colleagues in these teams).

And this is not only helping us get work started and finished much sooner, but is also giving career opportunities where traditional moves might not be possible.



In this vein our small group of ‘developing developers’ is building into a very successful community too. We recently conducted our first mob programming session, enabling us to share our own understanding and pick up new knowledge from others in the group.

We’ve also started a programme of short live demo sessions. Someone will ask a question and another member of the group will volunteer to provide a demo on the subject a couple of weeks later. I conducted the first, on JavaScript Promises, and although I’m certain the group learned very little, I learnt a load in preparing and delivering the session!

Other than that, the only progress I’ve made with my own flexi-skilling is to install zsh (a colourful shell is a must) and use Vim for the first time. Does that count?



It’s nice that the boundaries between my BA role and our wider engineering conversations are becoming more and more blurred, with my day job now centring more on coaching others in flexibility and experimentation, and how they can become confident in ever increasing new skills.

That said, it's been a pleasant change of pace to step back ever so slightly, and welcome our transitioning tech grads into our BA practice. And to spend time talking through and focusing on core BA capability in their development. It's also time for another brand new intake of graduates and I can't wait to share the organisational agility and flexible skills messages all over again.

Our grad schemes are the epitome of what we want to deliver with flexible skills, working through all of our tech teams before picking a specialism. And seeing the increasing difficulty they have in choosing a specialism area makes me think we're getting something right - helping to grow a wide appreciation and passion for all aspects of technology in retail. These are our new breed of properly t-shaped engineers, and ensuring a specialism doesn’t limit this breadth should be our number one priority.