---
title: "ATG Beginner Development Series. Part 1: Dev environment proxy setup"
date: 2020-09-23T08:15:00+01:00
categories: ["Posts"]
tags: ["Java", "ATG"]
image:
author: "mattTea"
draft: true
showtoc: false
---

_A fully detailed explanation of ATG dev environment setup and developer activities is provided here [Day to Day ATG11](https://www.jlpit.com/wiki/x/nYPPCQ). The following provides a more specific guide to proxy set up in the VM to allow access outside JLP networks._

<!-- more -->

------

## Access to Jenkins CI

In order to build ATG and related projects locally, access to the Jenkins CI server is required. This is at... https://ci.project4.com/jenkins_atg11/

There are 2 ways that access can be provided, via static ip access, or over Partnership vpn


### Static IP

With a static ip address set up on your network, you should request that this address is given access to `ci.project4.com` on Slack at the `#topic-rackspace` channel


### VPN

Without a static ip connection should be fine using a Partnership connected VPN (Cisco AnyConnect works for me)

------

### Proxy setup

- Test you can access https://ci.project4.com/jenkins_atg11/ on your host machine whilst connected to the vpn
- Then, if this works, within your ATG VM open Firefox `Menu` > `Preferences` > `Advanced` > `Network` > `Settings...`

- Select `Manual proxy configuration:`
- In `HTTP Proxy` enter `jlp-proxy.johnlewis.co.uk` and in `Port:` enter `80`
- Check the `Use this proxy for all protocols` box, and click `OK`

- Now when you open a tab and navigate to https://ci.project4.com/jenkins_atg11/ the Jenkins dashboard should load


If this works, you now need to configure the proxy to work for all your http requests with `Maven`

([This guide](http://maven.apache.org/guides/mini/guide-proxies.html) helps and provides the xml structure required)


- In a terminal on the VM, locate and open your `settings.xml` file

  ```bash
  vi ~/.m2/settings.xml
  ```

- You may see a `<mirrors>` entry, and you need to add a `<proxies>` entry as a sibling to mirrors in the file as follows (replacing your netdom id and password as necessary)...

  ```xml
  <mirrors>
    ...
  </mirrors>
  <proxies>
    <proxy>
        <id>jlp-proxy</id>
        <active>true</active>
        <protocol>https</protocol>
        <host>jlp-proxy.johnlewis.co.uk</host>
        <port>80</port>
        <username>netdomId</username>
        <password>netdomPassword</password>
      </proxy>
    </proxies>
  ```


- In a new terminal window (in the VM), run

  ```bash
  update-all
  ```


  This command will pull the latest code from SVN for all ATG related projects if the proxy setup has been successful.

  _(This assumes you have already checked out necessary SVN repositories. If this is not the case please follow the steps outlined in the **Get Latest Code** section [here](https://www.jlpit.com/wiki/x/nYPPCQ))._
