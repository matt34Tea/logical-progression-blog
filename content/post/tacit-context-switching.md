---
title: "Tacit Knowledge Series: 3. Calm Context Switching"
date: 2021-10-24T15:00:00+01:00
categories: ["Posts"]
tags: ["Learning", "Leadership"]
image: /img/switch.png
author: "mattTea"
draft: false
showtoc: false
---

*The [Tacit Knowledge Series](https://logical-progression.matttea.com/post/tacit-knowledge-series/) aims to understand expert engineers' approaches and how they reached their judgements in a number of day-to-day situations.*

------

The inspiration for this article came from seeing the ease with which some of my engineering idols switch, not just from context to context, but from deep detail in one context, to high-level, big picture views in different contexts.


## Scenario

There is not really a single scenario I can pick for this from my friendly engineering experts, as it happens multiple times per day. Every day. The scenario I generally find myself in though, is one where after joining another meeting or conversation I'm constantly catching my mind wandering from the current conversation back to the previous, usually still unsolved, problem I was discussing or thinking about immediately before.

I've looked around, and of course asked how my peers avoid this and apparently start and remain entirely focused on the new conversation and problem... 


## The knowledge

Well it turns out that it isn't as seamless and immediate as it appears on the surface. Possibly all of us struggle with context switching to some degree. And I did manage to pick up one or two tips for helping with this, and in some conversations we dived into a bit of theory and research to help us to recognise this problem and try to keep it front of mind when we start descending or wandering into this hole.


I have a pretty incomplete idea on a different topic that's currently bouncing around my mind; it's a long way from being entirely related, but has helped me frame this in the expert/non-expert context.  
I've started looking into why sometimes it can feel like you know less about a problem or project the longer you work with it. My theory is that it's entirely relative, based on the expertise and experience in the team. The following rough scribble hopefully highlights how over time the knowledge a more novice engineer gains over a time period becomes increasingly further away from the knowledge an expert may have gained over the same period...

![Knowledge growth over time based on experience for developers](/img/knowledge-growth.png)

The reason I'm including this here is that a secondary gap (time to reach the same knowledge) may create the *wandering mind* situation described above.  
Image one engineer takes 10 minutes longer than another engineer to get to the same point of understanding of a problem. Now also imagine that during that 10 minute window they both join another meeting and start discussing something else. If the first engineer is anything like me, their mind won't be able to rest until they get to that point of understanding, so they won't be able to fully focus on the next meeting. Possibly the second engineer, having reached the understanding, can park that train of thought, and be able to pay full attention in the next meeting.

Not sure how accurate this theory is, but one approach this generated to help with the context switching issue, is the idea of *micro-reflections*.  
Trying to take time for a micro-reflection at the end of each 'context', to gather your thought-threads together, or note where your thinking has reached, before diving into another context might help our minds to rest easy at the point that we reached, and allow us to pick the thread back up a later time, rather than while we should be focusing on something else. This might only need to be a few minutes.  


Another concept this threw into the conversation was an idea that maybe explains why our minds are doing this wandering, and if we can possibly harness this it might even be a super-power!  
In the chapter **This Is Your Brain** in [Refactor Your Wetware](https://pragprog.com/titles/ahptl/pragmatic-thinking-and-learning/), Andy Hunt's dual-processor brain analogy suggests our *second CPU* is similar to an asynchronous, pattern matching search engine. One that keeps chugging away in the background, while our *first CPU* can focus on the here and now. This might explain why we (or certainly I) have eureka moments while I'm cleaning my teeth or out for a run, moments when I'm certain I'm not trying to solve yesterday's code conundrum.  

I'm really interested in this idea, and it aligns with one of my favourite TED talks - [Adam Grant's Surprising Habits of Original Thinkers](https://www.ted.com/talks/adam_grant_the_surprising_habits_of_original_thinkers?referrer=playlist-talks_for_procrastinators#t-3989) in that we need to give our brain enough focused thought to enable it to do this background processing, or procrastination.


My very, very loose conclusion here goes something like this...  
- Take time for a micro-reflection between context switches
- Use this to note how far your thinking got, and maybe tie some of those loose ends together
- Most importantly give your brain this time, however short it is, to do some focused thinking
- And trust in your *supercomputer-second-CPU* to do the heavy lifting, and let the rest of you focus on that new context and problem...
