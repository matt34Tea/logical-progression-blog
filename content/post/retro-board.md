---
title: "Retro Board"
date: 2018-04-25T08:15:00+01:00
categories: ["Side Projects"]
tags: ["React", "Redux", "Learning"]
image: /img/reboot-camp.jpg
author: "mattTea"
draft: false
showtoc: false
---

A React workflow board web app with Redux and HTML drag and drop.

------

The code for the `Retro Board` app can be found at https://github.com/mattTea/RetroBoard
