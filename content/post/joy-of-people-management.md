---
title: "The Joy of People Management"
date: 2020-08-25T08:15:00+01:00
categories: ["Posts"]
tags: ["Leadership"]
image: /img/joy.jpg
author: "mattTea"
draft: false
showtoc: false
---

We can certainly be forgiven for fearing the worst and focusing on the negatives right now - a global pandemic that shows no signs of disappearing, Brexit just around the corner, and strategic reviews all around us at work - but being, or becoming a people manager shouldn't be one of them.

So, with all the change going on right now I'd like to share a few, maybe less obvious reasons why people management has been an overwhelmingly positive experience throughout my career.


## Time saving

This may sound completely counter-intuitive, but being a manager _saves_ me time.

Being a people manager, even to only a handful of people, immediately gives you access to a network that extends into each of their product areas and domains, and gives you an extremely candid insight into the personalities, the decisions, the challenges and most importantly the work in each of those areas.

For the last 18 months I've been fortunate enough to manage anywhere between 16 and 22 Partners, and far from this taking up all my time, it's provided me with so much insight into what's going on around our business that I rarely have to do any digging to determine who to speak to, or what the current goals or priorities are for a specific area.

A great recent example of this was when my own product team was trying to align priorities and roadmaps with others in the Findability domain, those of us in my People Management team who work in this domain were quickly able to get together and openly share our thoughts (and dare I say, our secrets!) and get to a great level of understanding far easier than we'd otherwise have been able to do.

This has proved even more valuable during lockdown and this 'new (ab)normal', because I now no longer have the luxury of walking around an area to catch conversations or people.


## Learning

I make no joke when I say I have learnt infinitely more from the people I've managed than they have from me.

Those whom I've managed have always been and will always be far better developers, analysts and engineers than I'll ever dream of being, but through working with, and coaching people to be even better I've been able to more clearly focus on areas for my own improvement. When working with other skilled professionals and working through their objectives and targets, you quickly become very attuned to what good looks like, and this is a huge benefit for your own career development.

At an individual skill level, you're surrounded by and regularly meeting the people in your team who are used to showing you what they can do and showing off their skills. It's far from a one way street, and I am equally coached on my problems and challenges by my team. There is no better example here than my own continuing journey in backend web development.


## A safe place to try things

The teams I've worked with have been wonderful at letting me try things out, they've not only been a safe space to get things wrong, they've also been amazingly honest at giving feedback and ideas - in many respects you represent your team, so they don't want you to look bad!

My team have been on the receiving end of draft versions of pretty much everything I've delivered at Practice meetings or external conferences, and have always been completely on point with the feedback they've given - thank you!


## People Management and individual team contribution are not mutually exclusive

The last point I'd like to share is that I've never felt that being a people manager and being a contributor in a team have ever been mutually exclusive parts of my job. I have many examples of where people management conversations have helped me unblock stuff in my team assignments and vice versa. My delivery team (hopefully!) benefit from having me as a person, the combined engineer and people manager that I am, and from me bringing all my skills to the team.
