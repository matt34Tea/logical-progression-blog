---
title: "Tacit Knowledge Series: 2. Bigger Picture Recall"
date: 2021-06-21T12:00:00+01:00
categories: ["Posts"]
tags: ["WebDev", "Design", "Learning"]
image: /img/recall.jpg
author: "mattTea"
draft: false
showtoc: false
---

*The [Tacit Knowledge Series](https://logical-progression.matttea.com/post/tacit-knowledge-series/) aims to understand expert engineers' approaches and how they reached their judgements in a number of day-to-day situations.*

------

This article looks at an area I'm regularly inspired by in my expert colleagues - the ability to recall the capabilities and integrations of multiple, continuously evolving applications.


## Scenario

I struggled to put this knowledge into words, but the expertise I see here is the ability to thoroughly understand the capabilities of a service or an application and being able to answer ongoing design and integration questions on the spot.

This commonly takes the form of questions from existing and potential consumers of a service, and the impacts of possible changes.
Equally, when a change is proposed or made, knowing which dependent teams need to be informed, and the impacts it will have on each of them.


## Examples

To try to pin down what I was talking about here so that I could point out and discuss these situations with the experts demonstrating them, I first set about gathering some examples. (And I'm continuing to do this, as this feels like the very essence of tacit knowledge in action).

I won't list them all here, but the examples (and many of the ensuing conversations) followed these themes...

- A question about a change another product team is planning to make, and knowing whether it's necessary to have an opinion about it
- Knowing the process order for a feature that results in an integration (for example, if data is transformed and upstream services are notified in an event-driven architecture, knowing the order of transformation, indexing and notification), so that impacts can be understood for each step
- Reasoning about determination logic in own product or downstream dependencies
- Data sources and consuming apis for own product and downstream dependencies


## The knowledge

Every expert I spoke to about this started with almost identical words - they all claimed to have terrible memories.

On digging a bit further into this, and using the specific examples in each case, they were able to elaborate a bit more around how they were able to recall capabilities and designs.

Familiar patterns and historical experience almost certainly play a part here, but the theme that emerged in helping recall in these situations was *structure*, or *applying logic*, rather than trying to remember. Put simply by one expert dev...

> *I start by asking myself what this thing does*  
> *What does it need to know in order to do that*  
> *And where does it get that from*  
>   
> *Asking these questions almost always gets me close to knowing what integrations are present and generally how it's designed*


Similarly, when pushed, another put it down to *rationale, order and structure*  


So thinking about ways I could reflect and try to add structure and logic to my ability to recall at this level, I thought back to concepts and areas where I generally do have better recall.  
At lower-level code functions and patterns I seem to do better at this, in part I believe because I take the time (very short though it often is) to reflect when I encounter something new, and write a few comments in my journal. This journal isn't for anyone's benefit other than my own, and I find the very simple act of reflecting and jotting something down helps me build a pathway to reasoning about it, and possibly *remembering*.

And this made me realise that I rarely, if ever, do this for higher-level and bigger picture stuff. So I started.

Each time I work on a specific area or service within our application now I try to add to an ever-growing slide deck that contains notes, photo'd scribbles, and sketches; all describing my view of what it does. This so far includes...

```txt
- Data sources
  - How a service or method is notified of new or updated data
  - Names of any storage buckets, and message topics and subscriptions
  - How it consumes the data  
  
- High level I/O to and from service layer methods
- Repositories and datastores
- Outgoing integrations (event driven or other)
- Scheduled tasks
- UI elements
```

And it certainly doesn't hurt to try and test this recall. Putting oneself in the position of having to answer the questions in the scenarios stated at the start of this article, to test and improve your understanding (and of course add to and change the scribbles I've so far built up). Regularly being in front of consumers and dependent teams. Being active, either presenting or learning from show and tells or code reviews.

And continuing to gather and analyse examples of experts doing this.