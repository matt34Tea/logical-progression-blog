---
title: "ATG Beginner Development Series. Part 2: Components & properties files"
date: 2020-09-24T08:15:00+01:00
categories: ["Posts"]
tags: ["Java", "ATG"]
image: 
author: "mattTea"
draft: true
showtoc: false
---

_The following outlines some structural concepts in Oracle ATG code that are necessary to understand in order to make changes to ATG components._

_This is one of my first experiences of ATG (and Java 7) development and is written from this perspective._

<!-- more -->

------

## What are components?

Oracle ATG Commerce (ATG) is assembled of component `beans` (based on ATG or custom Java classes) by linking them together through configuration files in a component model called `Nucleus`. This component model simply provides a way to organise ATG components, and enable them to interact. 

`Components` in ATG code (_generally classes that extend `nucleus` classes such as `GenericService`_) require a `.properties` file in order that the Nucleus component model can...

- Create and initialise a component from that properties file
- Allow components to point to one another through these properties (i.e. a component class may have a property that is set to a different component)


### Reference

[Oracle reference doc - organizing JavaBean components](https://docs.oracle.com/cd/E24152_01/Platform.10-1/ATGPlatformProgGuide/html/s0201nucleusorganizingjavabeancompone01.html)

[Oracle reference doc - specifying components as properties](https://docs.oracle.com/cd/E24152_01/Platform.10-1/ATGPlatformProgGuide/html/s0204specifyingcomponentsasproperties01.html)

------

## Component example

The following is an extract from an example component file, and highlights the structure and steps required for components to interact

1. Import dependencies (e.g. `import com.johnlewis.jec.website.catalog.SkuItemHelper;`)
2. Inject and name the dependencies (e.g. `private SkuItemHelper skuItemHelper;`)
3. Define `getter` and `setter` methods for the dependencies (either by using `Lombok` annotations at the top of the class, or by explicitly defining them at the end of the class - example shows both)

```java
package com.johnlewis.jec.website.catalog.export.json.mapper;

import atg.nucleus.GenericService;
import com.johnlewis.jec.website.catalog.SkuItemHelper;
import com.johnlewis.jec.website.catalog.export.json.helper.AdditionalServicesHelper;
import com.johnlewis.jec.website.catalog.export.json.helper.PricePopulator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdditionalServicesPopulator extends GenericService {

	private AdditionalServicesHelper additionalServicesHelper;
	private SkuItemHelper skuItemHelper;
	private PricePopulator pricePopulator;
	private boolean useCustomWarrantyData;

    // ...

    // alternative getter and setter definition...
    public PricePopulator getPricePopulator() {
        return pricePopulator;
    }

    public void setPricePopulator(PricePopulator pricePopulator) {
        this.pricePopulator = pricePopulator;
    }
}
```

------

## Properties file example

The example below shows the structure or the `.properties` file that relates to the above component. The key elements are...

- The component class and path it relates to (`AdditionalServicesPopulator`)
- Paths to other components that have pointers to them from this original component (e.g. `additionalServicesHelper`)
- Any initialisation property values (e.g. `useCustomWarrantyData`)


```properties
$class=com.johnlewis.jec.website.catalog.export.json.mapper.AdditionalServicesPopulator
$scope=global

additionalServicesHelper=/com/johnlewis/jec/website/catalog/export/json/helper/AdditionalServicesHelper
skuItemHelper=/com/johnlewis/jec/website/catalog/SkuItemHelper
pricePopulator=/com/johnlewis/jec/website/catalog/export/json/helper/PricePopulator
useCustomWarrantyData=true
```

------

## Folder Structure

Component `.properties` files must be separate from the component code, but follow the same folder structure as the components they configure


### Class folder structure

![Retro board example](/img/retro-board.png) USE THIS STYLE OF IMAGE LINK for below images

{{< figure src="/static/blogs/atg-dev-2-components-and-properties/class-folder-structure.png" alt="class folder structure" >}}

### Properties folder structure

{{< figure src="/static/blogs/atg-dev-2-components-and-properties/properties-folder-structure.png" alt="properties folder structure" >}}

------

## Error messages

If component and properties files are not stored in the correct locations, or connected correctly you may not see test failures, you may not even see build failures, but you will see errors when invoking methods (e.g. locally via `dyn/admin`)


### Examples

The following was thrown when trying to run the Product export method when the `.properties` file for `PricePopulator` didn't map the `PriceConverter` component correctly...

```bash
ERROR [nucleusNamespace] Unable to set configured property "/com/johnlewis/jec/website/catalog/export/json/mapper/PricePopulator.priceConverter" atg.nucleus.ConfigurationException: Unable to resolve component /com/johnlewis/jec/website/catalog/export/json/mapper/PriceConverter
```

The following was thrown when trying to run the Product export method when the `getter` or `setter` for the `PriceConverter` component was not defined...

```bash
[ExportJsonTask] [ExportJsonTask]::: Error Exporting Product 211212879 in Batch 500 due to Error null: java.lang.NullPointerException
	at com.johnlewis.jec.website.catalog.export.json.mapper.PricePopulator.convertedPriceValue(PricePopulator.java:12) [_dsm__jec__Website.common_sclasses_s:]
```

------

## Comments

Please do add any comments, thoughts and additions below - I'm always keen to learn more and improve my understanding
