---
title: "Tacit Knowledge Series: 1. Dependency Vulnerabilities"
date: 2021-04-10T15:00:00+01:00
categories: ["Posts"]
tags: ["WebDev", "Learning"]
image: /img/vulnerability.jpg
author: "mattTea"
draft: false
showtoc: false
---

*The [Tacit Knowledge Series](https://logical-progression.matttea.com/post/tacit-knowledge-series/) aims to understand expert engineers' approaches and how they reached their judgements in a number of day-to-day situations.*

------

This first article in the series looks at a common scenario for many of us - investigating dependency vulnerabilities - for the sole reason that I'm interested in how some of my more experienced colleagues could almost immediately determine the course of action to take, while many of us regularly lose hours to these investigations. A perfect tacit knowledge scenario I thought.


### Scenario

An automated pipeline or dependency checker alert highlights that a library in your app has a vulnerability. Let's use [CVE-2021-21295](https://nvd.nist.gov/vuln/detail/CVE-2021-21295) for this example. Security risks are obviously something that can't be ignored, so your carefully laid plans for the next 5 minutes, or the next hour, are ruined.

This is how the expert approach went...


### Principles and techniques

Certain principles and practices are a good idea to start with to understand the base information about the vulnerability. The [NIST National Vulnerability Database](https://nvd.nist.gov/vuln/detail/CVE-2021-21295) is a good place to start, providing background details and any known workarounds and fixes. Looking for links from this page to a [Github security advisory](https://github.com/netty/netty/security/advisories/GHSA-wm47-8v5p-wjpj) (or similar) for the affected library is also a good idea, as this can give more detail about how the vulnerability manifests, and areas of impact.

Things are usually simpler if the vulnerability is in a library that you use directly, and you can simply apply an updated version.

However, if it's an indirect dependency (which is more often than not), your tooling can help work out what's using it...
- If using Gradle build tools, the Gradle tab (in `IntelliJ`, for example) allows you to drill down the classpath trees to understand where the dependency is used, and if it's required at compile time or runtime
- An alternative is to pipe the `gradle dependencies` command into `less` and search for the offending library

```sh
gradle dependencies | less

...

/netty

=> +--- com.google.cloud:libraries-bom:16.1.0
=> |    +--- io.grpc:grpc-netty-shaded:1.33.1 (c)
```

Now we know where the library is used, we can go back to the nice helpful Github securities advisory page to understand the impact. The [example here](https://github.com/netty/netty/security/advisories/GHSA-wm47-8v5p-wjpj) suggests that users are only affected if certain `http2 codecs` are used to convert calls to `http1.1 objects` and then forwarded elsewhere.

So without the right tacit knowledge here we could head off to work through how our higher level and direct dependencies might be using this library, to see if this impact is true for any part of our application. Or...


### The knowledge

Whether this is tacit knowledge or good old experience is for someone else to decide, but an expert and speedy judgement was made in this case based on a few flags...

The security advisory impact highlighted that *users would only be affected if certain `http2 codecs` were used to convert calls to `http1.1 objects` and then forwarded elsewhere*. This sounds very much like proxy activity, which is further hinted at by the presence of a `Netflix Zuul` patch in the NIST detail - Zuul being an api gateway which is very likely to be doing exactly this activity. Are we doing any proxy activity in our app? No.

Looking at the direct dependencies that use our dodgy library, we probably have a greater understanding of what they're doing in our app. Picking `grpc` as the example from the terminal output above, we know that it's probably being used to initiate connections, and in this specific context, probably Google to Google connections.

With a little bit of thinking about our platform here too, we know that the app connections are in-cluster, with no exposed ports. We also know that no requests go through Netty, but through a managed ingress controller.

These quickly spotted flags were more than enough to provide a way forward with this vulnerability.
