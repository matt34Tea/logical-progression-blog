# mattTea

I like drinking tea. And coffee.

But while the water's heating I'm a people manager, product engineer and part of the tech profession leadership team at an awesome UK retailer.

My big areas of interest are web development, lean and agile product delivery and continuous learning. I work with incredible teams, love helping people get even better at what they do, and enjoy the occasional opportunity to share my thoughts at conferences in the UK and mainland Europe.

I use this site as a place to share my challenges, successes and most importantly, my learning experiences. 